.data
g: .dword 4
a: .dword 0x2, 0x3, 0x4, 0x5, 0x6, 0x7 //Arreglo de 6 elementos posiciones de 0 a 5, 0 a 40 
//saltos de 8 bits
dir: .dword 0x40080000 //Direccion de memoria del procesador


.text
LDR X1, g
LDR X6, = a // se le da la posicion de memoria 
LDR X4, dir
ADD X6, X6, X4 // suma la posicion de memoria con la del procesador

LDUR X9, [X6,#32]// x9 = 6
ADD X0, X9, X1 // x0=00...00 A
SUB X0, XZR, X0 // x0 = 11...116 

infloop: B infloop // loop infinito para que qemu no salga
