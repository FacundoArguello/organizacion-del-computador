
Configuración de la Memoria

Nombre           Origen             Longitud           Atributos
ram              0x0000000000000000 0x0000000000400000
*default*        0x0000000000000000 0xffffffffffffffff

Guión del enlazador y mapa de memoria

LOAD main.o

.text           0x0000000000000000       0x10
 *(.text*)
 .text          0x0000000000000000       0x10 main.o

.data           0x0000000000000010        0x8
 .data          0x0000000000000010        0x8 main.o

.bss            0x0000000000000018        0x0
 *(.bss*)
 .bss           0x0000000000000018        0x0 main.o
OUTPUT(main.elf elf64-littleaarch64)

.debug_line     0x0000000000000000       0x3b
 .debug_line    0x0000000000000000       0x3b main.o

.debug_info     0x0000000000000000       0x2e
 .debug_info    0x0000000000000000       0x2e main.o

.debug_abbrev   0x0000000000000000       0x14
 .debug_abbrev  0x0000000000000000       0x14 main.o

.debug_aranges  0x0000000000000000       0x30
 .debug_aranges
                0x0000000000000000       0x30 main.o

.debug_str      0x0000000000000000       0x5b
 .debug_str     0x0000000000000000       0x5b main.o
